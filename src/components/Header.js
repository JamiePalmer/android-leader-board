
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Button,Image,TouchableOpacity} from 'react-native';
export default class Header extends Component { 
  toggleDrawer = () => {   
    this.props.navigationProps.toggleDrawer(); 
  } 
  render() { 
    return ( 
      <View style={styles.container}>
        <Text style={styles.title}>{this.props.name}</Text>
        <TouchableOpacity 
         style={styles.menu}
         onPress={this.toggleDrawer.bind(this)} > 
            <View style={styles.bar}/>
            <View style={styles.bar}/>
            <View style={styles.bar}/> 
        </TouchableOpacity> 
      </View> 
    ); 
  }
}
const styles = StyleSheet.create({
  container:{ 
    flexDirection: 'row', 
    width:'100%',
    height:50,
    backgroundColor:'#242424' 
  },  
  menu:{
    position:'absolute',
    top:15,
    right:20
  },
  title:{
    color:'white',
    fontSize:25,
    marginLeft:15,
    marginTop:8,
  },

  bar:{
     width:25,
     height:4,
     marginBottom:4,     
     backgroundColor:'white'
  }
});
