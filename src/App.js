import React, { Component } from 'react';
import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox, Dimensions, WebView, ScrollView } from 'react-native';
import { createDrawerNavigator, createSwitchNavigator, createStackNavigator } from 'react-navigation';
import SigninScreen from './screens/auth/SigninScreen'
import SignupScreen from './screens/auth/SignupScreen'
import ForgotScreen from './screens/auth/ForgotScreen'
import HomeScreen from './screens/main/HomeScreen'
import CategoriesScreen from './screens/main/CategoriesScreen'
import RankScreen from './screens/main/RankScreen'
import UserinfoScreen from './screens/main/UserinfoScreen'
import DevinfoScreen from './screens/main/DevinfoScreen'
import Sub_CategoryScreen from './screens/main/Sub_CategoryScreen'
import ShopScreen from './screens/main/ShopScreen'
import firebase from 'react-native-firebase'

class CustomSideMenu extends Component {
  _signout() {
    firebase.auth().signOut().then(() => {
      this.props.navigation.navigate('SigninScreen', { status: 'signout' })
    })
  }
  render() {
    return (
      <View style={styles.sideMenuContainer}>
        <Image
          style={styles.logoimage}
          source={require('./assets/images/logo.png')}
        />
        <View style={styles.sideMenus}>
          <View style={styles.sideMenu}>
            <Image
              style={styles.sideMenuIcon}
              source={require('./assets/images/category.png')}
            />
            <Text style={styles.menuText} onPress={() => { this.props.navigation.navigate('CategoriesScreen') }} >Category</Text>
          </View>
          <View style={styles.sideMenu}>
            <Image
              style={styles.sideMenuIcon}
              source={require('./assets/images/user.png')}
            />
            <Text style={styles.menuText} onPress={() => { this.props.navigation.navigate('UserinfoScreen') }} >User Info</Text>
          </View>
          <View style={styles.sideMenu}>
            <Image
              style={styles.sideMenuIcon}
              source={require('./assets/images/develop.png')}
            />
            <Text style={styles.menuText} onPress={() => { this.props.navigation.navigate('DevinfoScreen') }} >Dev Info</Text>
          </View>
          <View style={styles.sideMenu}>
            <Image
              style={styles.sideMenuIcon}
              source={require('./assets/images/signout.png')}
            />
            <Text style={styles.menuText} onPress={() => { this._signout() }} >Sign out</Text>
          </View>
        </View>
      </View>
    );
  }
}
class Header extends Component {
  toggleDrawer = () => {
    this.props.navigationProps.toggleDrawer();
  }
  render() {
    return (
      <TouchableOpacity
        style={styles.menu}
        onPress={this.toggleDrawer.bind(this)} >
        <View style={styles.bar} />
        <View style={styles.bar} />
        <View style={styles.bar} />
      </TouchableOpacity>
    );
  }
}
const AuthStack = createStackNavigator({
  SigninScreen: {
    screen: SigninScreen,
    navigationOptions: ({ navigation }) => ({
      title: `Home`,
    }),
  },
  SignupScreen: { screen: SignupScreen },
  ForgotScreen: { screen: ForgotScreen },
},
  {
    navigationOptions: ({ navigation }) => {

      return {
        header: null
      };
    }
  }
);
const MainStack = createStackNavigator({
  HomeScreen: { screen: HomeScreen },
  CategoriesScreen: { screen: CategoriesScreen },
  RankScreen: { screen: RankScreen },
  UserinfoScreen: { screen: UserinfoScreen },
  DevinfoScreen: { screen: DevinfoScreen },
  ShopScreen: { screen: ShopScreen },
  Sub_CategoryScreen: { screen: Sub_CategoryScreen }
},
  {
    navigationOptions: ({ navigation }) => {
      return {
        headerRight: <Header navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#242424',

        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: 'white',
          marginLeft: 0
        },
      };
    }
  }
);

const MyDrawerNavigator = createDrawerNavigator(
  {
    MainStack: {
      screen: MainStack
    },
  },
  {
    contentComponent: CustomSideMenu,
    drawerWidth: 220,
  }
);

export default AppNavigator = createSwitchNavigator(
  {
    AuthStack: {
      screen: AuthStack
    },
    drawStack: {
      screen: MyDrawerNavigator
    },
  },
);

const styles = StyleSheet.create({

  MainContainer: {
    backgroundColor: 'white',          
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    alignItems: 'center',
    backgroundColor: "white",
    justifyContent: 'center',
  },
  sideMenuContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 20
  },
  logoimage: {
    marginTop: 20,
    width: 100,
    height: 100,
    borderRadius: 150 / 2
  },
  sideMenu:
  {
    margin: 10,
    width: '80%',
    flexDirection: 'row'
  },
  sideMenus: {
    marginTop: 20,
    width: '100%',
    alignItems: 'center'
  },
  sideMenuIcon:
  {
    resizeMode: 'center',
    width: 24,
    height: 24,
    marginRight: 10,
    marginLeft: 20
  },
  menuText: {
    fontSize: 16,
    color: '#222222',
  },
  menu: {
    marginRight: 15,
    marginTop: 8
  },
  bar: {
    width: 25,
    height: 4,
    marginBottom: 4,
    backgroundColor: 'white'
  },
});