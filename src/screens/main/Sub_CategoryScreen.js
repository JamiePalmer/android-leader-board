
import React, {Component} from 'react';
import {Platform, 
  StyleSheet, 
  Text, 
  View,
  Button ,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  //ScrollView
} from 'react-native';
import firebase from 'react-native-firebase'
import { 
  UIActivityIndicator 
} from 'react-native-indicators';
import { ScrollView } from 'react-native-gesture-handler';
export default class Category extends Component {
  static navigationOptions = {
    title: 'Category',    
  }; 
  constructor(props) { 
    super(props);        
    
  }
  state={ 
    category:'',   
    keys:null,
    data:null,
    food:[
      "Pizza","Kebab",'Burger','Asain','Indian',"Italian",'English','Mediterranean','Chicken','Mexican','Coffee'
    ],
    leisure:[
      'Club','Bar/Pub','Cinema','Gym'
    ],
    motors:[
      'Taxi','Garage','Gas'
    ],
    retail:[
      'Supermarket','Department','Clothes','Shoes','Health','Technology','Home','Jewellery'
    ],
    display:null,
  }
  componentDidMount(){
    this.setState({category: this.props.navigation.getParam('key')})
    this.display_data_init(this.props.navigation.getParam('key'));
    //this.props.navigation.navigate('RankScreen',{cate_key:this.props.navigation.getParam('key')})
  }
  display_data_init(key){
    switch (key) {
      case 'food':
         this.setState({display:this.state.food})
      break;
      case 'leisure':
         this.setState({display:this.state.leisure})
      break;
      case 'motors':
         this.setState({display:this.state.motors})
      break;
      case 'retail':
        this.setState({display:this.state.retail})
      break;
    
      default:
        break;
    }
  }
  render() {
   if(this.state.display!=null){
    return (          
      <ImageBackground
      resizeMode='stretch'
      source={require('../../assets/images/main/category.jpg')}
      style={styles.maincontainer}>
      <ScrollView style={{width:'100%',height:'100%'}}> 
      <View style={{width:'100%',flexDirection:'row',alignItems:'center',marginTop:30 }}>
      <View style={styles.category_box}> 
        {
          this.state.display.map((value, num) => {
           if(1){
               return (
               <TouchableOpacity
               key={num}
               onPress={() => { this.props.navigation.navigate('RankScreen',{cate_key:this.props.navigation.getParam('key'),sub_cate_key:value})}}
               style={styles.cate_btn}>                
                <Text style={styles.welcome}>{value}</Text>           
              </TouchableOpacity>
               )
           }
          }           
          )
        }
        </View>  
        
      </View>
      </ScrollView>
      </ImageBackground>
      
    );

   }else{
    return (
      <ImageBackground
      resizeMode='stretch'
      source={require('../../assets/images/main/category.jpg')}
      style={styles.container}>
       <UIActivityIndicator           
            color='white' />
      </ImageBackground>
    );
   }
  }
}

const styles = StyleSheet.create({
  container: {
    height:Dimensions.get('window').height-80,
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center',   
    flexDirection:'row'
  },
  maincontainer: {
    height:Dimensions.get('window').height-80,
    width:'100%',
    
  },

  category_box:{
    width:'100%',    
    alignItems: "center", 
    justifyContent:'center' 
    
  },

  cate_btn:{
  backgroundColor: "#79371A",
  width:'70%',
  margin:10,
  
  }, 
  cate_img:{
      width:Dimensions.get('window').width/2-50,
      height:Dimensions.get('window').width/2-80,
      
  } ,
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
    color: 'white',
  },
  instructions: {
    textAlign: 'center',
    color: 'blue',
    marginBottom: 5,

  },
});
