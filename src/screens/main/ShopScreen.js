
import React, {Component} from 'react';
import {Platform, 
  StyleSheet, 
  Text, 
  View,
  Button ,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground
} from 'react-native';

export default class Category extends Component {
  static navigationOptions = {
    title: 'Shop',    
  }; 
  constructor(props) { 
    super(props);   
     
    this.state={
     avatar:"https://firebasestorage.googleapis.com/v0/b/hotincants-a5d00.appspot.com/o/FoodLogo%2FDirectPizza.png?alt=media&token=7370d23b-814b-4a60-b4ea-0fd5f13b0536",
      shop_data:{
        logo_url:"https://firebasestorage.googleapis.com/v0/b/hotincants-a5d00.appspot.com/o/FoodLogo%2FDirectPizza.png?alt=media&token=7370d23b-814b-4a60-b4ea-0fd5f13b0536",
        location:'AB',
        company_name:'CT1 1BH',        
        phone_number:'01227 76778',
        operating_hours:'11:30-04:00',
        website:'topspizza.co.uk',
        created_at:'',
        category:''
       
      },
     
      
    }
  }
componentDidMount(){
  
 let data =this.props.navigation.getParam('shop_data')

this.setState({shop_data:data})
}
  render() {
    
    if(this.state.shop_data!=null){
      return (
        <ImageBackground
        resizeMode='stretch'
        source={require('../../assets/images/main/category.jpg')}
        style={styles.container}>
         <Text style={[styles.title,{margin:0}]}>{this.state.shop_data.company_name}</Text>
         <Image    
          resizeMode='stretch' 
          source={{uri:this.state.shop_data.logo_url}}
          style={styles.avatarimg}/>
        <View style={styles.items}>
         <Text style={styles.txt}>ADDRESS</Text>
         <Text style={styles.txt}>{this.state.shop_data.location}</Text>
        </View>
        <View style={styles.items}>
         <Text style={styles.txt}>PHONE</Text>
         <Text style={styles.txt}>{this.state.shop_data.phone_number}</Text>
        </View>
        <View style={styles.items}>
         <Text style={styles.txt}>CLOSING TIME</Text>
         <Text style={styles.txt}>{this.state.shop_data.operating_hours}</Text>
        </View>
        <View style={styles.items}>
         <Text style={styles.txt}>WEBSITE</Text>
         <Text style={styles.txt}>{this.state.shop_data.website}</Text>
        </View>
        <View style={styles.vote}>
        <View style={styles.vote_info}>          
            <Text style={styles.vote_txt}>WEEKLY VOTES AMOUNT</Text>
            <Text style={styles.vote_txt1}>{this.state.shop_data.weekly_votes}</Text>
        </View>
        <View style={styles.vote_info}>          
            <Text style={styles.vote_txt}>ALLTIME VOTES AMOUNT</Text>
            <Text style={styles.vote_txt1}>{this.state.shop_data.all_votes}</Text>
        </View>
        </View>
        
        </ImageBackground>
      );
    }else{
      return <Text>loading</Text>
    }
  }
}

const styles = StyleSheet.create({
  vote:{
      marginTop:40,
      width:'80%'
  },
  vote_info:{
  flexDirection:'row',
  width:'100%',
  margin:10
  },
  vote_txt:{
      width:'80%',
      color:'white',
      fontSize:18,
     
  },
  vote_txt1:{
    width:'20%',
    color:'white',
    fontSize:18,
   
},
items:{
    flexDirection:'row',
    width:'80%'
},
  container: {
    height:Dimensions.get('window').height-80,
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center',   
    
  },

title:{  
  color:'white',fontSize:20,
},
txt:{
  color:'white',fontSize:16,
  width:'40%',
  margin:10,
},

avatarimg:{
  width:120,
  height:120,
  marginBottom:50,
  marginTop:30,
  
},

  welcome: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  
});
