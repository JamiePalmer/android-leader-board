
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  //ScrollView
} from 'react-native';
import firebase from 'react-native-firebase'
import {
  UIActivityIndicator
} from 'react-native-indicators';
import { ScrollView } from 'react-native-gesture-handler';
export default class Category extends Component {
  static navigationOptions = {
    title: 'Categories',
  };
  constructor(props) {
    super(props);

  }
  state = {
    keys: null,
    data: [
      {
        key: 'food',
        name: 'Food',
        Logo_url: require('../../assets/images/food.png')
      },
      {
        key: 'leisure',
        name: 'Leisure',
        Logo_url: require('../../assets/images/leisure.png')
      },
      {
        key: 'motors',
        name: 'Motors',
        Logo_url: require('../../assets/images/motor.png')
      },
      {
        key: 'retail',
        name: 'Retail',
        Logo_url: require('../../assets/images/retail.jpg')
      }
    ]
  }
  componentDidMount() {

  }
  render() {
    return (
      <ImageBackground
        resizeMode='stretch'
        source={require('../../assets/images/main/category.jpg')}
        style={styles.container}>
        {
          this.state.data.map((value, num) => {
            if (1) {
              return (
                <TouchableOpacity
                  key={num}
                  onPress={() => { this.props.navigation.navigate('Sub_CategoryScreen', { key: value.key }) }}
                  style={styles.cate_btn}>
                  <ImageBackground
                    resizeMode={'stretch'}
                    style={styles.cate_img}
                    source={value.Logo_url} />
                  <View style={{ width: Dimensions.get('window').width / 2 - 50, height: '25%', backgroundColor: '#00000090', position: 'absolute', bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: 'white', fontSize: 16 }}>{value.name}</Text>
                  </View>
                </TouchableOpacity>
              )
            }
          }
          )
        }
      </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height - 80,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',

  },
  maincontainer: {
    height: Dimensions.get('window').height - 80,
    width: '100%',

  },

  leftbox: {
    width: "50%",
    alignItems: "center",
    borderRightWidth: 3,
    borderRightColor: 'white',

  },
  rightbox: {
    width: "50%",
    alignItems: "center",

  },
  cate_btn: {
    margin: 10
  },
  cate_img: {
    width: Dimensions.get('window').width / 2 - 50,
    height: Dimensions.get('window').width / 2 - 80,

  },
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
