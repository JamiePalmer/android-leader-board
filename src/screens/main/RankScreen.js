
import React, {Component} from 'react';
import {Platform, 
  StyleSheet, 
  Text, 
  View,
  Button ,
  Image,
  ImageBackground,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import firebase from 'react-native-firebase'
import Leaderboard from 'react-native-leaderboard';
import { 
  UIActivityIndicator 
} from 'react-native-indicators';
import Toast, {DURATION} from 'react-native-easy-toast'
export default class Rank extends Component { 
  static navigationOptions = {
    title: 'Rank',    
  };
  
  constructor(props) { 
    super(props); 
     
    this._GotoShop=this._GotoShop.bind(this) 
    this._Vote_status=this._Vote_status.bind(this)
  }
  state = {
    email_key:null,
    user_data:null,
    user_key:null, 
    status:'weekly',
    all_img:require('../../assets/images/main/rankwhite.png'),
    week_img:require('../../assets/images/main/rankbar.png'),
    all_color:'black',
    week_color:'white',   
    keys:null,
    rendering:null,
    cate_ky:'',
    flag_status:{
      food:[
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
      ],
      leisure:[
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
      ],
      motors:[
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
      ],
      retail:[
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
      ]
    },
    real_flag:null,
    vote_history:null,
    real_flag_history:null,
    data:null,
    display_data:[],

};
componentDidMount(){
  this.setState({cate_key:this.props.navigation.getParam('cate_key')})  
  
  firebase.database().ref('shop_table/').child(this.props.navigation.getParam('cate_key')).on('value', (snapshot) => {
    let data = snapshot.val();
    var display_data=Object.values(data);
    this.setState({ data: Object.values(data), keys: Object.keys(data)});
    display_data.map((value,num)=>{
      if( value.category==this.props.navigation.getParam('sub_cate_key')){
       this.state.display_data.push(value)
      }
    });
    this.setState({rendering:1})
    //alert(JSON.stringify(this.state.display_data))
  });
  this._user_data_fetch();
}
componentWillUnMount(){
  firebase.auth().removeAuthTokenListener(this.authListener);
}
_up_down_rendering(){
  firebase.database().ref('shop_table/').child(this.props.navigation.getParam('cate_key')).once('value', (snapshot) => {
    let data = snapshot.val();
    var display_data=Object.values(data);   
    this.state.data=Object.values(data);
    this.state.keys=Object.keys(data);
    this.state.display_data=[];
    display_data.map((value,num)=>{
      
      if( value.category==this.props.navigation.getParam('sub_cate_key')){
        
       this.state.display_data.push(value)
      }
    });
    this.setState({rendering:1})
    //alert(JSON.stringify(this.state.display_data))
  });
}
_flag_init(key){
  switch (key) {
    case 'food':
       this.setState({real_flag:this.state.flag_status.food,real_flag_history:this.state.vote_history.food})
    break;
    case 'leisure':
       this.setState({real_flag:this.state.flag_status.leisure,real_flag_history:this.state.vote_history.leisure})
    break;
    case 'motors':
       this.setState({real_flag:this.state.flag_status.motors,real_flag_history:this.state.vote_history.motors})
    break;
    case 'retail':
      this.setState({real_flag:this.state.flag_status.retail,real_flag_history:this.state.vote_history.retail})
    break;
  
    default:
      break;
  }

}

_flag_update(key){
  switch (key) {
    case 'food':       
       this.state.flag_status.food=this.state.real_flag;
       this.state.vote_history.food=this.state.real_flag_history;
    break;
    case 'leisure':
       this.state.flag_status.retail=this.state.real_flag;
       this.state.vote_history.retail=this.state.real_flag_history;
    break;
    break;
    case 'motors':
       this.state.flag_status.retail=this.state.real_flag;
       this.state.vote_history.retail=this.state.real_flag_history;
    break;
    break;
    case 'retail':
       this.state.flag_status.retail=this.state.real_flag;
       this.state.vote_history.retail=this.state.real_flag_history;
    break;
    break;
  
    default:
      break;
  }
  firebase.database().ref('users/').child(this.state.email_key).child(this.state.user_key).child('flag_status').set(JSON.stringify(this.state.flag_status))
  firebase.database().ref('users/').child(this.state.email_key).child(this.state.user_key).child('vote_history').set(JSON.stringify(this.state.vote_history))
}

_findReplaceAll(str){
  
  for (let index = 0; index < str.length; index++) {
   str=str.replace('.','-')  
  }
  return str;
}
_user_data_fetch() {   
  this.authListener=firebase.auth().onAuthStateChanged(user => {  
   if(user){     
    var email_key=this._findReplaceAll(user.email);
    this.state.email_key=email_key;
   }
    
   
  })
 
  firebase.database().ref('users/').child(this.state.email_key).on('value', (snapshot) => {
    let data = snapshot.val();
   
     this.setState({ user_data: Object.values(data)[0],user_key:Object.keys(data)[0]}); 
       
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var today=month+"/"+date+"/"+year;
    if(today!=Object.values(data)[0].today){
      firebase.database().ref('users/').child(this.state.email_key).child(this.state.user_key).child('today').set(today)
      firebase.database().ref('users/').child(this.state.email_key).child(this.state.user_key).child('flag_status').set(JSON.stringify(this.state.flag_status))
    }else{
      this.setState({flag_status:JSON.parse(this.state.user_data.flag_status)})
    }
    this.setState({vote_history:JSON.parse(this.state.user_data.vote_history)})

    this._flag_init(this.props.navigation.getParam('cate_key'));
  });
}
_user_total_votes_update(arg){
  if(arg=="1"){
    firebase.database().ref('users/').child(this.state.email_key).child(this.state.user_key).child('total_votes').set(this.state.user_data.total_votes+1)
  }else{
    firebase.database().ref('users/').child(this.state.email_key).child(this.state.user_key).child('total_votes').set(this.state.user_data.total_votes-1)
  }
  this._up_down_rendering();
}
 
 _GotoShop(icon){
  // alert(icon)
   this.state.data.map((value,num)=>{
    if(value.logo_url==icon){
      this.props.navigation.navigate('ShopScreen',{shop_data:value})
    }
   })
   
 } 
 _switch(me){
  if(me=='week'){
    this._up_down_rendering();
    this.setState({
      all_img:require('../../assets/images/main/rankwhite.png'),
      week_img:require('../../assets/images/main/rankbar.png'),
      all_color:'black',
      week_color:'white',
      status:'week'
     })
  }else{
   this._up_down_rendering();
   this.setState({
     week_img:require('../../assets/images/main/rankwhite.png'),
     all_img:require('../../assets/images/main/rankbar.png'),
     all_color:'white',
     week_color:'black',
     status:'all'
     
   })
  }
}

 _Vote_status(status, item){
this.state.data.map((value,num)=>{
 
  if(value.phone_number==item.phone_number){
    
       if(status=="1"&&this.state.real_flag[num]==1)   return   this._custom_toast('You have already voted(up) today.');    
    
      if(status=="-1"&&this.state.real_flag[num]==-1) return  this._custom_toast('You have already voted(down) today.');    
    
      if(status=="1") {
        this.state.real_flag[num]=this.state.real_flag[num]+1;
        this.state.real_flag_history[num]=this.state.real_flag_history[num]+1;
        this._flag_update(this.props.navigation.getParam('cate_key'))
      };
      if(status=="-1") {
        this.state.real_flag[num]=this.state.real_flag[num]-1;
        this.state.real_flag_history[num]=this.state.real_flag_history[num]-1;
        this._flag_update(this.props.navigation.getParam('cate_key'))
      }
      var date = new Date().getDate(); //Current Date
      var month = new Date().getMonth() + 1; //Current Month
      var year = new Date().getFullYear(); //Current Year
      var today=month+"/"+date+"/"+year;
      var all_votes=parseInt(item.all_votes)+parseInt(status);
      var category=item.category;
      var company_name=item.company_name;
      //var created_at=item.created_at
      var created_at=item.created_at;
         
      var location=item.location;
      var logo_url=item.logo_url;
      var operating_hours=item.operating_hours;
      var phone_number=item.phone_number;
      var website=item.website;
      var weekly_votes=parseInt(item.weekly_votes)+parseInt(status);
      
      firebase.database().ref('shop_table/').child(this.props.navigation.getParam('cate_key')).child(this.state.keys[num]).update({
        all_votes,
        category,
        company_name,
        created_at,
        location,
        logo_url,
        operating_hours,
        phone_number,
        website,
        weekly_votes
      }).then((data) => {
        //success callback
        if(status=="1"){
          firebase.database().ref('shop_table/').child(this.props.navigation.getParam('cate_key')).child(this.state.keys[num]).child('created_at').push({        
            today        
          }).then((data) => {
            //success callback
            
           
          }).catch((error) => {
            //error callback
            console.log('error ', error)
           
          })
          this._user_total_votes_update("1");
        }else{
          firebase.database().ref('shop_table/').child(this.props.navigation.getParam('cate_key')).child(this.state.keys[num]).child('created_at').child(Object.keys(created_at)[0]).remove();
          this._user_total_votes_update("-1");
        }
        
       
      }).catch((error) => {
        //error callback
        console.log('error ', error)
       
      })
    
  
   // this.setState({rendering:1}) 
    
  
  }
})
}
_custom_toast(txt){
  this.refs.toast_notification.show(txt, 800, () => {
   console.log('toast')
  });  
}
render() {
  if(this.state.rendering!=null&&this.state.real_flag!=null){
   return (     
     <View styles={styles.container}>
       <View style={{width:'100%',height:45,flexDirection:'row',backgroundColor:'white'}}>
          <TouchableOpacity
          style={{width:'50%',height:'100%'}}
          onPress={()=>{this._switch('week')}}
          >
          < ImageBackground           
          style={{width:'100%',height:'100%',alignItems:'center',justifyContent:'center'}}
          source={this.state.week_img}
          >
          <Text style={{fontSize:16,color:this.state.week_color}}>WEEKLY</Text>
          </ImageBackground>
          </TouchableOpacity>
          <TouchableOpacity
          style={{width:'50%',height:'100%'}}
          onPress={()=>{this._switch('all')}}
          >
          
           <ImageBackground
          style={{width:'100%',height:'100%',alignItems:'center',justifyContent:'center'}}
          source={this.state.all_img}
          >
          <Text style={{fontSize:16,color:this.state.all_color}}>ALL TIME</Text>
          </ImageBackground>
          </TouchableOpacity>

       </View>
       <ImageBackground
       source={require('../../assets/images/main/category.jpg')}
       style={styles.maincontainer}>
         <Text style={styles.category_txt}>{this.props.navigation.getParam('sub_cate_key')}</Text>
        
         {  
         (()=>{
           if(this.state.status=='all'){                                   
             return   <Leaderboard 
                       GotoShop={this._GotoShop}
                       Vote_status={this._Vote_status}
                       data={this.state.display_data} 
                       icon={'logo_url'}
                       sortBy={'all_votes'} 
                       labelBy={'company_name'}
                       />
           } else{
             return   <Leaderboard 
                       GotoShop={this._GotoShop}
                       Vote_status={this._Vote_status}
                       data={this.state.display_data} 
                       icon={'logo_url'}
                       sortBy={'weekly_votes'} 
                       labelBy={'company_name'}
                       />
           }                                  
         })()
             
         }
         <Toast ref="toast_notification"
        style={{backgroundColor:'#000000',width:280,height:40,borderRadius:20,alignItems:'center',justifyContent: 'center',bottom:60}}        
        
        textStyle={{color:'white',fontSize:16,textAlign:'center'}}
       />   
       </ImageBackground>
     </View>
   );
  }else{
    return (
     < ImageBackground 
     source={require('../../assets/images/main/category.jpg')}          
     style={styles.container}>
     <UIActivityIndicator         
      color='white' />
     
     </ImageBackground>
    );
  }
 }
}

const styles = StyleSheet.create({
  // scene: {
  //   width:"100%",
  //   height:"100%",
  //   flex: 1,
  // },
  container: {
    height:Dimensions.get('window').height-80,
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center', 
    backgroundColor:'red'  
    
  },
  maincontainer:  {
    height:Dimensions.get('window').height-125,
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center', 
    backgroundColor:'red'  
    
  },
  category_txt:{
    width:'100%',
    textAlign:'center',
    color:'white',
    fontSize:25,
    marginTop:10,
    fontWeight:'bold'
  }
});