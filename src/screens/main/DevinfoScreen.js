
import React, {Component} from 'react';
import {Platform, 
  StyleSheet, 
  Text, 
  View,
  Button ,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground
} from 'react-native';
import Toast, {DURATION} from 'react-native-easy-toast'
export default class Category extends Component {
  static navigationOptions = {
    title: 'Developer',    
  }; 
  constructor(props) { 
    super(props);   
     
    this.state={
     
      user_data:{
        avatar:require('../../assets/images/main/dev.png'),
        dev_name:'Vikram Singh',
        email:'vs271@kent.ac.uk',
        unisversity:'University of Kent',
        course:'Multi-media techonology and design',
        software_info_url1:require('../../assets/images/main/react.png'),
        software_info_url2:require('../../assets/images/main/android.png'),
        software_info_url3:require('../../assets/images/main/node.png')
      }
    }
  }
  _custom_toast(txt){
  
    this.refs.toast_notification.show(txt, 800, () => {
     console.log('toast')
    });  
  }
  render() {
    
    return (
      <ImageBackground
      resizeMode='stretch'
      source={require('../../assets/images/main/category.jpg')}
      style={styles.container}>
       <Toast ref="toast_notification"
        style={{backgroundColor:'#000000',width:150,height:40,borderRadius:20,alignItems:'center',justifyContent: 'center',bottom:60}}        
        
        textStyle={{color:'white',fontSize:16,textAlign:'center'}}
       />
       <Text style={[styles.title,{margin:0}]}>DEVELOPER INFORMATION</Text>
       <Image     
      source={this.state.user_data.avatar}
      style={styles.avatarimg}/>
      <View style={{flexDirection:'row'}}>
       <Text style={styles.txt}>Developer Name</Text>
       <Text style={styles.txt1}>{this.state.user_data.dev_name}</Text>
      </View>
      <View style={{flexDirection:'row'}}>
       <Text style={styles.txt}>Email</Text>
       <Text style={styles.txt1}>{this.state.user_data.email}</Text>
      </View>
      <View style={{flexDirection:'row'}}>
       <Text style={styles.txt}>University</Text>
       <Text style={styles.txt1}>{this.state.user_data.unisversity}</Text>
      </View>
      <View style={{flexDirection:'row'}}>
       <Text style={styles.txt}>Course Name</Text>
       <Text style={styles.txt1}>{this.state.user_data.course}</Text>
      </View>
     
      <View style={styles.software_info}>
      <Text style={styles.software_txt}>SOFTWARE USED</Text>
       <View style={{flexDirection:'row',}}>
       <TouchableOpacity
       onPress={()=>{this._custom_toast("React Native")}}
       >
        <Image     
          source={this.state.user_data.software_info_url1}
          style={styles.shop_img}/>
      </TouchableOpacity>
      <TouchableOpacity
      onPress={()=>{this._custom_toast("Android Studio")}}
      >
         <Image     
        source={this.state.user_data.software_info_url2}
        style={styles.shop_img}/>
        </TouchableOpacity>
      <TouchableOpacity
      onPress={()=>{this._custom_toast("Node.js")}}
      >
         <Image     
        source={this.state.user_data.software_info_url3}
        style={styles.shop_img}/>
      </TouchableOpacity>
        
       </View>

        
      </View> 
        
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height:Dimensions.get('window').height-80,
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center',   
    
  },

title:{  
  color:'white',fontSize:20,fontWeight:'bold'
},
txt:{
  color:'white',fontSize:15,
  width:'38%',
  margin:5,
},
txt1:{
  color:'white',fontSize:15,
  width:'42%',
  margin:5,
},

avatarimg:{
  width:150,
  height:150,
  borderRadius: 150/2,
  marginBottom:50,
  marginTop:30,
  
},
software_info:{
  width:'85%',
  height:140,
  backgroundColor:'white',
  borderWidth:3,
  borderColor:'#000000f0',  
  alignItems:'center',
  justifyContent:'center',
  marginTop:10
},
shop_img:{
  margin:15,
  width:70,
  height:70,
  borderRadius: 70/2,
  backgroundColor:'green'

},
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  software_txt:{
    color:'white',fontSize:16,color:'black',
    marginTop:10
  }
});
