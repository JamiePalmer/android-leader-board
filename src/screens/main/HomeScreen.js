
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground
} from 'react-native';
import firebase from 'react-native-firebase'
export default class Home extends Component {
  static navigationOptions = {
    title: 'Home',
    headerTitleStyle: {
      marginLeft: 25
    },

  };
  constructor(props) {
     super(props);
  }
  state = {
    data: null,
    keys: null,

  } 
  _findReplaceAll(str) {

    for (let index = 0; index < str.length; index++) {
      str = str.replace('.', '-')
    }
    return str;
  }
  componentDidMount() {
    this.server_manage();
  }

  async server_manage() {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var today = month + "/" + date + "/" + year;
    this.state.category.map((item, num) => {
      firebase.database().ref('shop_table/').child(item).once('value', (snapshot) => {
        let data = snapshot.val();
        //alert(JSON.stringify(data))
        this.setState({ data: Object.values(data), keys: Object.keys(data) });
        this.state.keys.map((shop_key, num1) => {

          firebase.database().ref('shop_table/').child(item).child(shop_key).child('created_at').once('value', (snapshot1) => {
            let vote_data = snapshot1.val();
            Object.values(vote_data).map((time, index) => {
              var olddate = new Date(Object.values(time)[0])
              var weekdate = new Date(today)
              if (olddate == "3/11/2019") {
                // No delete 
              }
              else if (weekdate - olddate > 604800000 - 1) {               
                firebase.database().ref('shop_table/').child(item).child(shop_key).child('created_at').child(Object.keys(vote_data)[index]).remove();
                firebase.database().ref('shop_table/').child(item).child(shop_key).child('weekly_votes').set(Object.keys(vote_data).length - 1);
              }
            })
          })
        })

      });

    })

  }
  render() {
    return (
      <ImageBackground
        resizeMode='stretch'
        source={require('../../assets/images/main/main.png')}
        style={styles.container}>

        <View style={styles.maincontainer}>
          <Text style={[styles.welcome, { fontSize: 14 }]}>Welcome to Hot-in-Cants!</Text>
          <Text style={styles.welcome}>This app allows you to vote based on liking - Up or Down, on a service you tried today in Canterbury.</Text>
          <Text style={styles.welcome}>Use this app to find out what is the best in Canterbury. Weekly leader board will show you what is the best in the current week, but you can also view All-time leader board to show what is the best overtime!</Text>
          <Text style={styles.welcome}>This app will provide you information about the service if you interested on operating details.</Text>
          <Text style={[styles.welcome, { fontSize: 14 }]}>Happy voting </Text>
        </View>

      </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
  container: {   
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  maincontainer: {
    position: 'absolute',
    bottom: 50,
    width: '70%',
    backgroundColor: 'white',
    borderRadius: 10,
    height: 250,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    borderColor: 'black',
    borderWidth: 1

  },
  welcome: {
    fontSize: 12,   
    margin: 2,
    color: 'black'

  },

});


