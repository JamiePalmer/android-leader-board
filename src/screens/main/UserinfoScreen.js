
import React, {Component} from 'react';
import {Platform, 
  StyleSheet, 
  Text, 
  View,
  Button ,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground
} from 'react-native';
import firebase from 'react-native-firebase'
//import SQLite from 'react-native-sqlite-2';
import { 
  UIActivityIndicator 
} from 'react-native-indicators';

export default class Category extends Component {
  static navigationOptions = {
    title: 'User',    
  }; 
  constructor(props) { 
    super(props);   
     
    
  }
  state={
    email_key:null, 
    fav_shop_n:null,  
    fav_shop_avatar:null,
    user_data:{
      vote_history:null,
      user_avatar:'https://firebasestorage.googleapis.com/v0/b/hotincants-a5d00.appspot.com/o/Images%2Fuser.png?alt=media&token=7938d521-786e-4719-af38-4905bb6e4301',
      total_votes:0,
      fav_shop_n:null,
      fav_shop_avatar:"https://firebasestorage.googleapis.com/v0/b/hotincants-a5d00.appspot.com/o/FoodLogo%2FDirectPizza.png?alt=media&token=7370d23b-814b-4a60-b4ea-0fd5f13b0536"
    }
  }
  componentWillUnMount(){
    firebase.auth().removeAuthTokenListener(this.authListener);
  }
  componentDidMount(){
   this._user_data_fetch();
  }
  _findReplaceAll(str){
  
    for (let index = 0; index < str.length; index++) {
     str=str.replace('.','-')  
    }
    return str;
  }
_user_data_fetch() {   
  this.authListener=firebase.auth().onAuthStateChanged(user => { 
    //alert(JSON.stringify(user))   
   if(user!=null){  
     
    var email_key=this._findReplaceAll(user.email)       
   // alert(email_key)
    this.state.email_key=email_key;
    
   }else{
   //default
    this.state.email_key="tom202221@gmail-com";
   }
   
  })
  
 
  firebase.database().ref('users/').child(this.state.email_key).on('value', (snapshot) => {
    let data = snapshot.val();   
     this.setState({ user_data: Object.values(data)[0]}); 
    // alert(JSON.stringify( Object.values(data)[0])) 
    this.state.user_data=Object.values(data)[0];
    this.state.vote_history=JSON.parse(this.state.user_data.vote_history)
    //favorite shop getting by the history of the user's vote
    var _array_food=this.state.vote_history.food; 
    var food_max_val=Math.max.apply(Math,_array_food);
    var food_maxindex=_array_food.indexOf(food_max_val);
    var _array_leisure=this.state.vote_history.leisure;  
    var leisure_max_val=Math.max.apply(Math,_array_leisure);
    
    var leisure_maxindex=_array_leisure.indexOf(leisure_max_val);
    var _array_motors=this.state.vote_history.motors;  
     
    var motors_max_val=Math.max.apply(Math,_array_motors);
    var motors_maxindex=_array_motors.indexOf(motors_max_val);   
    var _array_retail=this.state.vote_history.retail; 
    var retail_max_val=Math.max.apply(Math,_array_retail);
    var retail_maxindex=_array_retail.indexOf(retail_max_val);
    var max_category=[food_max_val,leisure_max_val,motors_max_val,retail_max_val]; 
    var category_index=0;
    if(Math.max.apply(Math,max_category)==0||null){
      category_index=4;
    }else{
      category_index=max_category.indexOf(Math.max.apply(Math,max_category)) }
  
    switch (category_index) {
      case 0:
      firebase.database().ref('shop_table/').child('food').on('value', (snapshot) => {
        let data = snapshot.val();               
        this.setState({fav_shop_avatar:Object.values(data)[food_maxindex].logo_url,fav_shop_n:Object.values(data)[food_maxindex].company_name})
      });
      break;
      case 1:
      firebase.database().ref('shop_table/').child('leisure').on('value', (snapshot) => {
        let data = snapshot.val();        
        this.setState({fav_shop_avatar:Object.values(data)[leisure_maxindex].logo_url,fav_shop_n:Object.values(data)[leisure_maxindex].company_name})
      });
      break;
      case 2:
      firebase.database().ref('shop_table/').child('motors').on('value', (snapshot) => {
        let data = snapshot.val();        
        this.setState({fav_shop_avatar:Object.values(data)[motors_maxindex].logo_url,fav_shop_n:Object.values(data)[motors_maxindex].company_name})
      });
      break;
      case 3:
      firebase.database().ref('shop_table/').child('retail').on('value', (snapshot) => {
        let data = snapshot.val();        
        this.setState({fav_shop_avatar:Object.values(data)[retail_maxindex].logo_url,fav_shop_n:Object.values(data)[retail_maxindex].company_name})
      });
      break;
      case 4:
      this.setState({fav_shop_avatar:"https://firebasestorage.googleapis.com/v0/b/hotincants-a5d00.appspot.com/o/Images%2Flogo.png?alt=media&token=18cd12b0-d59c-46fd-a4f8-2948a1dc8c13",
      fav_shop_n:'NO'})
      break;     
    
      default:  
     
      break;
    }
    


    
    

  })
  ;
}
  render() {
  if(this.state.fav_shop_n!=null){
    return (
      <ImageBackground
      resizeMode='stretch'
      source={require('../../assets/images/main/category.jpg')}
      style={styles.container}>
       <Text style={[styles.title,{margin:20}]}>USER INFO</Text>
       <Image     
      source={{uri:this.state.user_data.user_avatar}}
      style={styles.user_avatarimg}/>
      <View style={{flexDirection:'row',marginBottom:30}}>
       <Text style={[styles.title,{margin:20}]}>NUMBER OF VOTES</Text>
       <Text style={[styles.title,{margin:20}]}>{this.state.user_data.total_votes}</Text>
      </View>
      <View style={styles.favo_shop}>
        <Image     
        source={{uri:this.state.fav_shop_avatar}}
        style={styles.shop_img}/>
        <View  style={{marginTop:20}}>
          <Text style={[styles.title,{color:'black'}]}>FAVOURITE SHOP</Text>
          <Text style={[styles.title,{marginTop:15,color:'black',width:'80%'}]}>{this.state.fav_shop_n}</Text>
        </View>
      </View> 
        
      </ImageBackground>
    );
  }else{
    return < ImageBackground 
            source={require('../../assets/images/main/category.jpg')}          
            style={styles.container}>
            <UIActivityIndicator         
            color='white' />
    
         </ImageBackground>
  }
  }
}

const styles = StyleSheet.create({
  container: {
    height:Dimensions.get('window').height-80,
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center',   
    
  },

title:{
  color:'white',fontSize:20,fontWeight:'bold',
},
user_avatarimg:{
  width:150,
  height:150,
  borderRadius: 150/2,
  marginBottom:50,
  marginTop:30,
  
},
favo_shop:{
  width:320,
  height:120,
  backgroundColor:'white',
  borderWidth:3,
  borderColor:'#000000f0',
  flexDirection:'row'
},
shop_img:{
  margin:15,
  width:90,
  height:90,
  backgroundColor:'green'

},
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  
});
