

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  TextInput
} from 'react-native';
import firebase from 'react-native-firebase'
import {
  UIActivityIndicator
} from 'react-native-indicators';
import ImagePicker from 'react-native-image-picker'
import { ScrollView } from 'react-native-gesture-handler';
const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_avatar: 'https://firebasestorage.googleapis.com/v0/b/hotincants-a5d00.appspot.com/o/Images%2Fuser.png?alt=media&token=7938d521-786e-4719-af38-4905bb6e4301',
      indicator: 'none',
      username: "",
      firstname: '',
      lastname: '',
      email: "",
      password: "",
      repassword: "",
      errorMessage: '',
      phone: '',
      total_votes: 0,
      fav_shop_n: 'NO',
      fav_shop_avatar: 'https://firebasestorage.googleapis.com/v0/b/hotincants-a5d00.appspot.com/o/Images%2Flogo.png?alt=media&token=18cd12b0-d59c-46fd-a4f8-2948a1dc8c13',
      //fav_shop_avatar:"https://firebasestorage.googleapis.com/v0/b/hotincants-a5d00.appspot.com/o/Images%2Fdownload%20(1)sss.jpg?alt=media&token=e0e46646-52c2-4a48-99a0-8bac762861f7"
      flag_status: {
        food: [
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ],
        leisure: [
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ],
        motors: [
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ],
        retail: [
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        ]
      },
      items: [
        {
          id: 0,
          name: "First Name",
          secur: false
        },
        {
          id: 1,
          name: "Last Name",
          secur: false
        },
        {
          id: 2,
          name: "Email",
          secur: false
        },
        {
          id: 3,
          name: "Phone Number",
          secur: false
        },

        {
          id: 4,
          name: "Password",
          secur: true
        },
        {
          id: 5,
          name: "Confirm Password",
          secur: true
        },


      ]
    }


  }
  componentDidMount() {

    this.requestCameraPermission();
  }

  _pickImage() {

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {        
        this.setState({ user_avatar: `data:image/png;base64,${response.data}` });
      }
    });
  }

  async  requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          'title': 'Cool Photo App Camera Permission',
          'message': 'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.'
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera")
      } else {
        console.log("Camera permission denied")
      }
    } catch (err) {
      console.warn(err)
    }
  }
  _handleSignUp = (firstname, lastname, email, phone, password, repassword) => {

    if (password != repassword) return alert('Please confirm password');
    if (firstname == "" || lastname == "" || email == "" || phone == "" || password == "" || repassword == "") return alert("Please Input  User's Information");
    this.setState({ indicator: 'flex' })

    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => this._Insert_usertable(firstname, lastname, email, phone, this.state.total_votes, this.state.fav_shop_n, this.state.fav_shop_avatar, this.state.user_avatar, JSON.stringify(this.state.flag_status), JSON.stringify(this.state.flag_status)))
      .catch(error =>
        //alert(error.message)
        this.setState({ errorMessage: error.message })
      )

  }
  _findReplaceAll(str) {

    for (let index = 0; index < str.length; index++) {
      str = str.replace('.', '-')
    }
    return str;
  }
  _Insert_usertable(firstname, lastname, email, phone, total_votes, fav_shop_n, fav_shop_avatar, user_avatar, flag_status, vote_history) {
    var email_key = this._findReplaceAll(email)
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var today = month + "/" + date + "/" + year;
    firebase.database().ref('users/').child(email_key).push({
      firstname, lastname, email, phone, total_votes, fav_shop_n, fav_shop_avatar, user_avatar, flag_status, today, vote_history
    }).then((data) => {
      //success callback
      this.setState({ indicator: 'none' })

    }).catch((error) => {
      //error callback
      this.setState({ indicator: 'none' })

      console.log('error ', error)
    })
  }
  handleInput = (text, index) => {
    switch (index) {
      case 0:
        this.setState({ firstname: text })
        break;
      case 1:
        this.setState({ lastname: text })
        break;
      case 2:
        this.setState({ email: text })
        break;
      case 3:
        this.setState({ phone: text })
        break;
      case 4:
        this.setState({ password: text })
        break;
      case 5:
        this.setState({ repassword: text })
        break;

      default:
        break;
    }

  }
  render() {
    return (
      <ScrollView style={{ width: '100%', height: '100%' }}>
        <ImageBackground
          source={require('../../assets/images/auth/bg_img.png')}
          style={styles.container}>

          <TouchableOpacity
            onPress={() => { this._pickImage() }}
            style={styles.logo}>
            <Image
              source={{ uri: this.state.user_avatar }}
              style={styles.logoimg} />
          </TouchableOpacity>
          <View style={styles.maincontainer}>
            {
              this.state.items.map((item, index) => (
                <TextInput
                  key={index}
                  style={styles.iteminput}
                  underlineColorAndroid="transparent"
                  placeholder={item.name}
                  placeholderTextColor="#ffffff"
                  autoCapitalize="none"
                  secureTextEntry={item.secur}
                  onChangeText={(text) => this.handleInput(text, index)}
                />
              ))
            }
            <TouchableOpacity
              style={styles.auth_btn}
              onPress={() => { this._handleSignUp(this.state.firstname, this.state.lastname, this.state.email, this.state.phone, this.state.password, this.state.repassword) }}
            >
              <Text style={styles.btn_txt}>Sign Up</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => { this.props.navigation.navigate('SigninScreen') }}>
              <Text style={[styles.btn_txt_bold, { marginTop: 30 }]}

              >Already have an account? Sign In</Text>
            </TouchableOpacity>
            <UIActivityIndicator
              style={{ display: this.state.indicator, marginTop: 30 }}
              color='white' />

          </View>



        </ImageBackground>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height,
    width: '100%',
    alignItems: 'center',

    backgroundColor: '#F5FCFF',
  },
  scrollcontainer: {
    //width:'100%',
    alignItems: 'center'
  },
  logo: {
    position: 'absolute',
    top: 50,
    width: 120,
    height: 120,
    borderRadius: 60
  },
  logoimg: {
    width: 120,
    height: 120,
    borderRadius: 60
  },
  iteminput: {
    width: '90%',
    marginTop: 3,
    height: 45,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    fontSize: 17,
    color: 'white'
  },
  maincontainer: {
    marginTop: 180,
    alignItems: 'center',
    width: '70%',

  },
  auth_btn: {
    marginTop: 50,
    borderRadius: 10,
    width: '90%',
    height: 35,
    backgroundColor: '#676cfb',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btn_txt: {
    color: 'white',
    fontSize: 14
  },
  btn_txt_bold: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  }
});




//"https://firebasestorage.googleapis.com/v0/b/hotincants-a5d00.appspot.com/o/Images%2Fdownload%20(1)sss.jpg?alt=media&token=e0e46646-52c2-4a48-99a0-8bac762861f7"
