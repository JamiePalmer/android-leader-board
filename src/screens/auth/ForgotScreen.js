

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  TextInput
} from 'react-native';
import {
  UIActivityIndicator
} from 'react-native-indicators';
import firebase from 'react-native-firebase'
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      errorMessage: '',
      indicator: 'none',
    }

  }
  // this is the function to sent request for Reset password to user's email
  _handleResetpassword(email) {
    if (email == '') return alert('Please Input Your Email');
    this.setState({ indicator: 'flex' })
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => this.setState({ errorMessage: 'Sent New Password to your Email', indicator: 'none' }))
      .catch(error => this.setState({ errorMessage: error.message, indicator: 'none' })

      )
  }
  handleInput = (text) => {
    this.setState({ email: text });
  }
  render() {
    return (
      <ImageBackground
        source={require('../../assets/images/auth/bg_img.png')}
        style={styles.container}>
        <ImageBackground
          source={require('../../assets/images/auth/logo.png')}
          style={styles.logo}></ImageBackground>
        <View style={styles.maincontainer}>
          <TextInput
            style={styles.iteminput}
            underlineColorAndroid="transparent"
            placeholder={'Your Email'}
            placeholderTextColor="#ffffff"
            autoCapitalize="none"
            secureTextEntry={false}
            onChangeText={(text) => this.handleInput(text)}
          />
          <TouchableOpacity
            style={styles.auth_btn}
            onPress={() => { this._handleResetpassword(this.state.email) }}
          >
            <Text style={styles.btn_txt}>Send New Password</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('SigninScreen') }}>
            <Text style={[styles.btn_txt_bold, { marginTop: 30 }]}
            >Do you have new password? Sign In</Text>
          </TouchableOpacity>
          {
            <Text style={{ marginTop: 50, color: 'white', textAlign: 'center' }}>{this.state.errorMessage}</Text>
          }
          <UIActivityIndicator
            style={{ display: this.state.indicator, marginTop: 10 }}
            color='white' />

        </View>


      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height,
    width: '100%',
    alignItems: 'center',

    backgroundColor: '#F5FCFF',
  },
  logo: {
    position: 'absolute',
    top: 50,
    width: 120,
    height: 120
  },
  iteminput: {
    width: '90%',
    marginTop: 3,
    height: 45,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    fontSize: 17,
    color: 'white'
  },
  maincontainer: {
    marginTop: 180,
    alignItems: 'center',
    width: '70%',

  },
  auth_btn: {
    marginTop: 50,
    borderRadius: 10,
    width: '90%',
    height: 35,
    backgroundColor: '#676cfb',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btn_txt: {
    color: 'white',
    fontSize: 14
  },
  btn_txt_bold: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  }
});
