
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  TextInput,
  AsyncStorage
} from 'react-native';
import firebase from 'react-native-firebase'
import {
  UIActivityIndicator
} from 'react-native-indicators';
import SplashScreen from 'react-native-splash-screen'
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: null,
      indicator: 'none',
      email: "",
      password: "",
      errorMessage: '',
      logined: "flase",
      user_infromation: [null, null],
      items: [
        {
          id: 0,
          name: "Email",
          secur: false
        },
        {
          id: 1,
          name: "Password",
          secur: true
        }
      ]
    }
  }
  componentDidMount() {    
    SplashScreen.hide();    
    this.authListener = firebase.auth().onAuthStateChanged(user => {
      if (user != null) {
        this.props.navigation.navigate('HomeScreen')
      }
    })

  }
  componentWillUnMount() {
    firebase.auth().removeAuthTokenListener(this.authListener);
  }

  _handlesignin(email, password) {
    if (email == "" || password == "") return 0;
    if (email == null || password == null) return 0;

    this.setState({ indicator: 'flex' })
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => this.props.navigation.navigate('HomeScreen'))
      .catch(error => this.setState({ errorMessage: error.message, indicator: 'none' })
      )
  }

  handleInput = (text, index) => {
    this.state.user_infromation[index] = text;

  }

  render() {
    return (
      <ImageBackground
        source={require('../../assets/images/auth/bg_img.png')}
        style={styles.container}>
        <ImageBackground
          source={require('../../assets/images/auth/logo.png')}
          style={styles.logo}></ImageBackground>
        <View style={styles.maincontainer}>
          {
            this.state.items.map((item, index) => (
              <TextInput
                key={index}
                style={styles.iteminput}
                underlineColorAndroid="transparent"
                placeholder={item.name}
                placeholderTextColor="#ffffff"
                autoCapitalize="none"
                secureTextEntry={item.secur}
                onChangeText={(text) => this.handleInput(text, index)}
              />
            ))
          }
          <TouchableOpacity
            style={styles.auth_btn}
            onPress={() => { this._handlesignin(this.state.user_infromation[0], this.state.user_infromation[1]) }}
          >
            <Text style={styles.btn_txt}>Sign In</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('SignupScreen') }}>
            <Text style={[styles.btn_txt_bold, { marginTop: 40 }]}
            >Do not have account? Sign up</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('ForgotScreen') }}>
            <Text style={[styles.btn_txt_bold, { marginTop: 30 }]}
            >Forgot your Password?</Text>
          </TouchableOpacity>
          {
            <Text style={{ marginTop: 30, color: 'white', textAlign: 'center' }}>{this.state.errorMessage}</Text>
          }
          <UIActivityIndicator
            style={{ display: this.state.indicator, marginTop: 10 }}
            color='white' />
        </View>


      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height,
    width: '100%',
    alignItems: 'center',

    backgroundColor: '#F5FCFF',
  },
  logo: {
    position: 'absolute',
    top: 50,
    width: 120,
    height: 120
  },
  iteminput: {
    width: '90%',
    marginTop: 5,
    height: 50,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    fontSize: 17,
    color: 'white'
  },
  maincontainer: {
    marginTop: 180,
    alignItems: 'center',
    width: '70%',

  },
  auth_btn: {
    marginTop: 50,
    borderRadius: 10,
    width: '90%',
    height: 35,
    backgroundColor: '#676cfb',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btn_txt: {
    color: 'white',
    fontSize: 14
  },
  btn_txt_bold: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  }
});
