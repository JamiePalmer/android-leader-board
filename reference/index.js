import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  ListView,
  ViewPropTypes,
  Image,
  TouchableOpacity,
  StyleSheet
} from "react-native";

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const oddRowColor = "white";
const evenRowColor = "#f2f5f7";

export default class Leaderboard extends Component {
  state = {
    sortedData: []
  };

  static propTypes = {
    ...ViewPropTypes,
    //required
    data: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    sortBy: PropTypes.string.isRequired,
    labelBy: PropTypes.string.isRequired,

    //optional
    sort: PropTypes.func,
    icon: PropTypes.string,
    onRowPress: PropTypes.func,
    renderItem: PropTypes.func,
    GotoShop:PropTypes.func,
    Vote_status:PropTypes.func,
    containerStyle: PropTypes.object,
    scoreStyle: PropTypes.object,
    rankStyle: PropTypes.object,
    labelStyle: PropTypes.object,
    avatarStyle: PropTypes.object,
    oddRowColor: PropTypes.string,
    evenRowColor: PropTypes.string
  };

  _sort = data => {
    const sortBy = this.props.sortBy;

    let sorted = [];
    if (this.props.sort) {
      return this.props.sort(data);
    } else if (typeof data === "object") {
      let sortedKeys =
        data &&
        Object.keys(data).sort((key1, key2) => {
          return data[key2][sortBy] - data[key1][sortBy];
        });
      return (
        sortedKeys &&
        sortedKeys.map(key => {
          return data[key];
        })
      );
    } else if (typeof data === "array") {
      return (
        data &&
        data.sort((item1, item2) => {
          return item2[sortBy] - item1[sortBy];
        })
      );
    }
  };

  _defaultRenderItem = (item, index) => {
    const sortBy = this.props.sortBy;
    const evenColor = this.props.evenRowColor || evenRowColor;
    const oddColor = this.props.oddRowColor || oddRowColor;

    const rowColor = index % 2 === 0 ? evenColor : oddColor;

    const rowJSx = (
     <TouchableOpacity
     onPress={()=>{this.props.GotoShop(item[this.props.icon])}}
  
     >
      <View style={[styles.row, { backgroundColor: rowColor }]} key={index}>
        <View style={styles.left}>
          <Text
            style={[
              styles.rank,
              this.props.rankStyle,
              index < 9 ? styles.singleDidget : styles.doubleDidget
            ]}
          >
            Rank {parseInt(index) + 1}
          </Text>
          {this.props.icon && (
            <Image
              resizeMode={'stretch'}
              source={{ uri: item[this.props.icon] }}
              style={[styles.avatar, this.props.avatarStyle]}
            />
          )}
          <Text style={[styles.label, this.props.labelStyle]} numberOfLines={1}>
            {item[this.props.labelBy]}
          </Text>
          <View style={{width:100,height:80,flexDirection:'row'}}>
           <View style={{width:65,alignItems:'center',justifyContent:'center'}}>
             <Text style={{color:'black',fontSize:14}}>VOTE</Text>
             <Text style={[styles.score, this.props.scoreStyle]}>
                {item[sortBy] || 0}
             </Text>
           </View>
           <View style={{width:35,alignItems:'center',justifyContent:'center'}}>
             <TouchableOpacity
              style={{width:30,height:30,margin:5}}
              onPress={()=>{this.props.Vote_status(1,item)}}
             >
             <Image
             
             source={require('./up.png')}
             style={{width:'100%',height:'100%'}}
             />
             </TouchableOpacity>
             <TouchableOpacity
              style={{width:30,height:30,margin:5}}
              onPress={()=>{this.props.Vote_status(-1,item)}}
             >
              <Image
             source={require('./down.png')}
             style={{width:'100%',height:'100%'}}
             />
             </TouchableOpacity>
           </View>          
          
        </View>  
        </View>
            
        
      </View>
      </TouchableOpacity>
    );

    return this.props.onRowPress ? (
      <TouchableOpacity onPress={e => this.props.onRowPress(item, index)}>
        {rowJSx}
      </TouchableOpacity>
    ) : (
      rowJSx
    );
  };

  _renderItem = (item, index) => {
    return this.props.renderItem
      ? this.props.renderItem(item, index)
      : this._defaultRenderItem(item, index);
  };

  componentWillMount() {
    this.setState({ sortedData: this._sort(this.props.data) });
  }

  componentWillReceiveProps = nextProps => {
    if (this.props.data !== nextProps.data) {
      this.setState({ sortedData: this._sort(nextProps.data) });
    }
  };

  render() {
    const dataSource = ds.cloneWithRows(this.state.sortedData);

    return (
      <ListView
        style={this.props.containerStyle}
        dataSource={dataSource}
        renderRow={(data, someShit, i) => this._renderItem(data, i)}
        enableEmptySections
      />
    );
  }
}

const styles = StyleSheet.create({
  row: {
    paddingTop: 0,
    paddingBottom: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderWidth: 3,
    borderColor:'#ffffff',
    borderRadius: 5,    
    width:350,
    margin:10,
    
  },
  left: {
    flexDirection: "row",
    alignItems: "center"
  },
  rank: {
    fontSize: 15,
    fontWeight: "bold",
    marginRight: 5,
    color:'black'
  },
  singleDidget: {
    paddingLeft: 16,
    paddingRight: 6
  },
  doubleDidget: {
    paddingLeft: 10,
    paddingRight: 2
  },
  label: {
    fontSize: 18,
    fontWeight:'bold',    
    width:125,
   
    textAlign:'center',
    color:'black'
    
  },
  score: {
    marginTop:10,
    fontSize: 14,
    color:'black'   
   
  },
  avatar: {
    height: 35,
    width: 35,
    borderWidth:1,
    borderColor:'green',

    marginRight: 10
  }
});
